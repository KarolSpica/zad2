package tvseries;

import java.util.List;

import season.Seasons;


	public class TvSeries {
		
		private String name;
		private List<Seasons> seasons;
		
		public TvSeries(String name) {
			this.name = name;
		}
		public TvSeries(String name, List<Seasons> seasons) {
			this.name = name;
			this.seasons = seasons;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<Seasons> getSeasons() {
			return seasons;
		}
		public void setSeasons(List<Seasons> seasons) {
			this.seasons = seasons;
		}
		@Override
		public String toString() {
			return "TvSeries [name=" + name + ", seasons=" + seasons + "]";
		}
		
		
		

	}

