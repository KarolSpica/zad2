package tvseries;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.karol.zadaniedwa.Connect;

import season.Seasons;
import season.SeasonsMethod;


public class TvSeriesMethod {

	private PreparedStatement addTvSeriesStmt;
	private PreparedStatement deletAllTvSeriesStmt;
	private PreparedStatement getAllTvSeriesStmt;
	
	public List<TvSeries> pobierzWszystkie(Connect polaczenie){
		List<TvSeries> tvSeries = new ArrayList<TvSeries>();
		
		try {
			getAllTvSeriesStmt = polaczenie.connection.prepareStatement("SELECT id,nazwa FROM TVSERIES");
			ResultSet resultSet = getAllTvSeriesStmt.executeQuery();
			
			while(resultSet.next()) {
				TvSeries tv = new TvSeries(resultSet.getString("nazwa"));
				tvSeries.add(tv);
				}
					
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tvSeries;
	}
	public void usuwanie(Connect polaczenie) {
		try {
			deletAllTvSeriesStmt = polaczenie.connection.prepareStatement("DELETE FROM TVSERIES");
			deletAllTvSeriesStmt.executeUpdate();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public int dodawanie(Connect polaczenie, TvSeries tvSeries) {
		int count = 0;
		SeasonsMethod addSeason = new SeasonsMethod();
		try {
			addTvSeriesStmt = polaczenie.connection.prepareStatement("INSERT INTO TVSERIES (nazwa) VALUES(?)");
			addTvSeriesStmt.setString(1, tvSeries.getName());
			count = addTvSeriesStmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		if(tvSeries.getSeasons()!=null) {
		for(Seasons seasons : tvSeries.getSeasons()) {
			addSeason.addSeason(polaczenie, seasons, tvSeries.getName());
		}
		}
		return count;
	}

}
