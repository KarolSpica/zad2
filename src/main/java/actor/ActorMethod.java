package actor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.karol.zadaniedwa.Connect;

public class ActorMethod {
	
	private PreparedStatement addActorStmt;
	private PreparedStatement deletAllActorsStmt;
	private PreparedStatement getAllActorsStmt;
	private PreparedStatement checkTvSeriesId;
	private PreparedStatement checkActorId;
	private PreparedStatement addActorTvSeries;
	public List<Actor> getAllActors(Connect polaczenie){
		List<Actor> actors = new ArrayList<Actor>();
			
			try {
				getAllActorsStmt = polaczenie.connection.prepareStatement("SELECT nazwisko, dataUrodzenia, biografia FROM ACTOR ");
				ResultSet resultSet = getAllActorsStmt.executeQuery();
				
				while(resultSet.next()) {
					Actor actor = new Actor(resultSet.getString("nazwisko"),
							resultSet.getDate("dataUrodzenia"),
							resultSet.getString("biografia"));
					
					actors.add(actor);
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
			return actors;
	}
	public void clearActor(Connect polaczenie) {
		try {
			deletAllActorsStmt = polaczenie.connection.prepareStatement("DELETE FROM ACTOR");
			deletAllActorsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int addActor(Connect polaczenie,Actor actor, String nameSeries) {
		int count = 0;
		int kluczObcy = 0;
		int kluczAktora = 0;
		ResultSet resultSet;
		java.sql.Date dataSQL = new java.sql.Date(actor.getDateOfBirth().getTime());
		try {
			addActorStmt = polaczenie.connection.prepareStatement("INSERT INTO ACTOR (nazwisko, dataUrodzenia, biografia, idserialu) VALUES(?,?,?,?)");
			checkTvSeriesId = polaczenie.connection.prepareStatement("SELECT id FROM TVSERIES WHERE nazwa=(?)");
			checkTvSeriesId.setString(1, nameSeries);
			resultSet = checkTvSeriesId.executeQuery();
			while(resultSet.next()) {
				kluczObcy = resultSet.getInt(1);
			}
			
			addActorStmt.setString(1, actor.getName());
			addActorStmt.setDate(2,dataSQL);
			addActorStmt.setString(3, actor.getBiography());
			addActorStmt.setInt(4, kluczObcy);
			
			count = addActorStmt.executeUpdate();
			 
			checkActorId = polaczenie.connection.prepareStatement("SELECT id FROM ACTOR WHERE nazwisko=(?)");
			checkActorId.setString(1, actor.getName());
			resultSet = checkActorId.executeQuery();
			while(resultSet.next()) {
				kluczAktora = resultSet.getInt(1);
			}
			addActorTvSeries = polaczenie.connection.prepareStatement("INSERT INTO BRIDG (idactor, idserialu) VALUES(?,?)");
			addActorTvSeries.setInt(1, kluczAktora);
			addActorTvSeries.setInt(2, kluczObcy);
			
			count = addActorTvSeries.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

}
