package director;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.karol.zadaniedwa.Connect;


public class DirectorMethod {


	private PreparedStatement addDirectorStmt;
	private PreparedStatement deletAllDirectorsStmt;
	private PreparedStatement getAllDirectorStmt;
	
	public List<Director> getAllDirectors(Connect polaczenie){
		List<Director> directors = new ArrayList<Director>();
		
		try {
			getAllDirectorStmt = polaczenie.connection.prepareStatement("SELECT nazwisko, urodzony, biografia FROM DIRECTOR");
			ResultSet resultSet = getAllDirectorStmt.executeQuery();
			
			while(resultSet.next()) {
				Director director = new Director(resultSet.getString("nazwisko"),
						resultSet.getDate("urodzony"),
						resultSet.getString("biografia"));
		
				
				directors.add(director);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return directors;
	}
	
	public void clearDirector(Connect polaczenie) {
		try {
			deletAllDirectorsStmt = polaczenie.connection.prepareStatement("DELETE FROM DIRECTOR");
			deletAllDirectorsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public int addDirector(Connect polaczenie, Director director) {
		int count = 0;
		java.sql.Date dataSQL = new java.sql.Date(director.getDateOfBirth().getTime());
		try {
			addDirectorStmt = polaczenie.connection.prepareStatement("INSERT INTO DIRECTOR (nazwisko,urodzony,biografia) VALUES(?,?,?)"); 
			addDirectorStmt.setString(1, director.getName());
			addDirectorStmt.setDate(2, dataSQL);                 
			addDirectorStmt.setString(3, (director.getBiography()));
			
			count = addDirectorStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	
}
