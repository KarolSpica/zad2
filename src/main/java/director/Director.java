package director;

import java.util.Date;
public class Director {
	
	private String name;
	private Date dateOfBirth;
	private String biography;
	
	
	public Director(String name, Date dateOfBirth, String biography) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.biography = biography;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	@Override
	public String toString() {
		return "Director [name=" + name + ", dateOfBirth=" + dateOfBirth + ", biography=" + biography + "]";
	}
	
	

}
