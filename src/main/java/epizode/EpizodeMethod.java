package epizode;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.karol.zadaniedwa.Connect;

public class EpizodeMethod {
	
	private PreparedStatement addEpisodeStmt;
	private PreparedStatement deletAllEpisodesStmt;
	private PreparedStatement getAllEpisodesStmt;
	private PreparedStatement checkTvSeriesId;
	private PreparedStatement checkSeasonId;
	
	public List<Epizode> getAllEpisodes(Connect polaczenie){
		List<Epizode> episods = new ArrayList<Epizode>();
		
		try {
			getAllEpisodesStmt = polaczenie.connection.prepareStatement("SELECT nazwa, dataWydania, numerOdcinka, czasTrwania FROM EPISODE");
			ResultSet resultSet = getAllEpisodesStmt.executeQuery();
			
			while(resultSet.next()) {
				Epizode episode = new Epizode(resultSet.getString("nazwa"),
						resultSet.getDate("dataWydania"),
						resultSet.getInt("numerOdcinka"),
						resultSet.getInt("czasTrwania"));
		
				episods.add(episode);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return episods;
	}
	void clearEpisodes(Connect polaczenie) {
		try {
			deletAllEpisodesStmt = polaczenie.connection.prepareStatement("DELETE FROM EPISODE");
			deletAllEpisodesStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int addEpisode(Connect polaczenie,Epizode episode, int sezonNumber, String nazwaSerialu) {
		int count = 0;
		int idSezonu=0;
		int idSerialu = 0;
		ResultSet resultSet;
		java.sql.Date dataSQL = new java.sql.Date(episode.getReleaseDate().getTime());
		try {
			checkSeasonId = polaczenie.connection.prepareStatement("SELECT Id FROM SEASON WHERE numerSezonu=(?) AND idserialu=(?) ");
			checkTvSeriesId = polaczenie.connection.prepareStatement("SELECT id FROM TVSERIES WHERE nazwa=(?)"); //blad????
			addEpisodeStmt = polaczenie.connection.prepareStatement("INSERT INTO EPISODE (nazwa, dataWydania, numerOdcinka, czasTrwania, idsezonu) VALUES(?,?,?,?,?)");
			checkTvSeriesId.setString(1, nazwaSerialu);
			resultSet = checkTvSeriesId.executeQuery();
			while(resultSet.next()) {
				idSerialu = resultSet.getInt(1);
			}
			
			
			checkSeasonId.setInt(1, sezonNumber);
			checkSeasonId.setInt(2, idSerialu);
			resultSet = checkSeasonId.executeQuery();
			while(resultSet.next()) {
				idSezonu= resultSet.getInt(1);
			}
			
			
			addEpisodeStmt.setString(1, episode.getName() );
			addEpisodeStmt.setDate(2, dataSQL);
			addEpisodeStmt.setInt(3, episode.getEpisodeNumber());
			addEpisodeStmt.setInt(4, episode.getDuration());
			addEpisodeStmt.setInt(5, idSezonu);
			
			count = addEpisodeStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
}
