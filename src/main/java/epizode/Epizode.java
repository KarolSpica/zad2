package epizode;

import java.util.Date;

public class Epizode {
	
	private String name;
	private Date releaseDate;
	private int episodeNumber;
	private int duration;
	
	public Epizode(String name, Date releaseDate, int episodeNumber, int duration) {
		this.name = name;
		this.releaseDate = releaseDate;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getEpisodeNumber() {
		return episodeNumber;
	}
	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	@Override
	public String toString() {
		return "Epizode [name=" + name + ", releaseDate=" + releaseDate + ", episodeNumber=" + episodeNumber
				+ ", duration=" + duration + "]";
	}
	
	
}