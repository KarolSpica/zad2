package season;

import java.util.List;

import epizode.Epizode;

public class Seasons {
	
	private int seasonNumber;
	private int yearOfRelease;
	private List<Epizode> episodes;
	
	
	public Seasons(int seasonNumber, int yearOfRelease) {
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
	}
	public Seasons(int seasonNumber, int yearOfRelease, List<Epizode> epizodes) {
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
		this.episodes = epizodes;
	}
	public int getSeasonNumber() {
		return seasonNumber;
	}
	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}
	public List<Epizode> getEpisodes() {
		return episodes;
	}
	public void setEpisodes(List<Epizode> episodes) {
		this.episodes = episodes;
	}
	@Override
	public String toString() {
		return "Seasons [seasonNumber=" + seasonNumber + ", yearOfRelease=" + yearOfRelease + ", episodes=" + episodes
				+ "]";
	}
	
	
	
	
}
