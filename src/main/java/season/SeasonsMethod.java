package season;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.karol.zadaniedwa.Connect;

import epizode.Epizode;
import epizode.EpizodeMethod;

public class SeasonsMethod {

	private PreparedStatement addSeasonStmt;
	private PreparedStatement deletAllSeasonsStmt;
	private PreparedStatement getAllSeasonsStmt;
	private PreparedStatement checkTvSeriesId;
	
	public List<Seasons> getAllSeasons(Connect polaczenie){
		List<Seasons> seasons = new ArrayList<Seasons>();
		
		try{
			getAllSeasonsStmt = polaczenie.connection.prepareStatement("SELECT numerSezonu, rokWydania FROM SEASON");
			ResultSet resultSet = getAllSeasonsStmt.executeQuery();
			
			while(resultSet.next()) {
				Seasons season = new Seasons(resultSet.getInt("numerSezonu"),resultSet.getInt("rokWydania"));
				seasons.add(season);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seasons;
	}
	void clearSeasons(Connect polaczenie) {
		try {
			deletAllSeasonsStmt = polaczenie.connection.prepareStatement("DELETE FROM SEASON ");
			deletAllSeasonsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int addSeason(Connect polaczenie,Seasons season, String nameSeries) {
		int count = 0;
		int kluczObcy=0;
		EpizodeMethod addEpizode = new EpizodeMethod();
		ResultSet resultSet;
		try {
			addSeasonStmt = polaczenie.connection.prepareStatement("INSERT INTO SEASON (numerSezonu, rokWydania, idserialu) VALUES(?,?,?)");
			checkTvSeriesId = polaczenie.connection.prepareStatement("SELECT id FROM TVSERIES WHERE nazwa=(?)");
			checkTvSeriesId.setString(1, nameSeries);
			resultSet = checkTvSeriesId.executeQuery();
			while(resultSet.next()) {
				kluczObcy = resultSet.getInt(1);
			}
			
			addSeasonStmt.setInt(1,season.getSeasonNumber());
			addSeasonStmt.setInt(2, season.getYearOfRelease());
			addSeasonStmt.setInt(3, kluczObcy);
			count = addSeasonStmt.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		if(season.getEpisodes()!=null) {
		for(Epizode epizod : season.getEpisodes()) {
				addEpizode.addEpisode(polaczenie, epizod, season.getSeasonNumber(), nameSeries);
			}
		}
		return count;
		
	}
}
