package com.karol.zadaniedwa;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import actor.Actor;
import actor.ActorMethod;

public class ActorMethodTest {

	Connect connection = new Connect();
	ActorMethod aktorMethod = new ActorMethod();
	
	private final static String name = "Aktor";
	private final static String biography = "Aktor";
	private final static Date data = new Date(90-01-02);
	private final static String seriesName = "Dexter"; 
	@Test
	public void checkAddingActor() {
		Actor aktor = new Actor(name,data,biography);
		aktorMethod.addActor(connection, aktor, seriesName );
		
		List<Actor> actors = aktorMethod.getAllActors(connection);
		Actor actorRetrived = actors.get(0);
		
		assertEquals(name, actorRetrived.getName());
		assertEquals(biography, actorRetrived.getBiography());
	
	}

}
